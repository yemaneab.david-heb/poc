package com.example.demo.controller;

import com.example.demo.dto.ParentDto;
import com.example.demo.dto.PatientDto;
import com.example.demo.entity.Book;
import com.example.demo.entity.Child;
import com.example.demo.entity.Parent;
import com.example.demo.entity.prescription.PrescriptionEntity;
import com.example.demo.repository.ParentRepository;
import com.example.demo.service.BookService;
import com.example.demo.service.ChildService;
import com.example.demo.service.TestService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TestController {
    final
    TestService service;
    final BookService bookService;

    final ChildService childService;

    final ParentRepository parentRepository;
    public TestController(TestService service, BookService bookService, ChildService childService, ParentRepository parentRepository) {
        this.service = service;
        this.bookService = bookService;
        this.childService = childService;
        this.parentRepository = parentRepository;
    }

    @PostMapping("/parents")
    @Operation(summary = "Get a hello message")
    public ParentDto createUser(@RequestBody ParentDto parentDto) {
        return parentDto;
    }

    @PatchMapping(path = "/parent/{id}", consumes = "application/json-patch+json")
    @Operation(summary = "Get a hello message")
    public ResponseEntity<ParentDto> patchUpdate(@PathVariable Long id, @RequestBody JsonPatch patch) throws JsonPatchException, JsonProcessingException {
        ParentDto targetParent = service.getParentById(id);
        ParentDto parentDto = applyPatchToParent(patch, targetParent);
        return ResponseEntity.ok(service.updateParent(parentDto));
    }

    @PatchMapping( "/parent")
    @Operation(summary = "Get a hello message")
    public ResponseEntity<ParentDto> patchUpdate(@RequestBody ParentDto parentDto) throws JsonPatchException, JsonProcessingException {
        return ResponseEntity.ok(service.updateParent(parentDto));

    }
    @PostMapping( "/parent")
    @Operation(summary = "Get a hello message")
    public ResponseEntity<ParentDto> postUpdate(@RequestBody ParentDto parentDto) throws JsonPatchException, JsonProcessingException {
        return ResponseEntity.ok(service.updateParent(parentDto));
    }

    @PutMapping( "/parent")
    @Operation(summary = "Get a hello message")
    public ResponseEntity<ParentDto> putUpdate(@RequestBody ParentDto parentDto) throws JsonPatchException, JsonProcessingException {
        return ResponseEntity.ok(service.updateParent(parentDto));
    }

    @GetMapping("/parent/{id}")
    @Operation(summary = "Get a hello message")
    public ParentDto get(@PathVariable Long id){
        return service.getParentById(id);
    }


    @GetMapping("/parent-e/{id}")
    @Operation(summary = "Get a hello message")
    public ParentDto gete(@PathVariable Long id){
        ParentDto parent = service.getLazyParent(id);


    //  List<Child> children = parent.getChildren();

        parent.getChildren().stream().findAny().get().setAge(689);
        return parent;
    }

    @GetMapping("/children/{id}")
    @Operation(summary = "Get a hello message")
    public List<Child> getChildren(@PathVariable Long id){
        Parent parent = parentRepository.findById(id).get();
        System.out.println(parent.getChildren().size());
        return parentRepository.findById(id).get().getChildren();
    }



    private ParentDto applyPatchToParent(
            JsonPatch patch, ParentDto targetParent) throws JsonPatchException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode patched = patch.apply(mapper.convertValue(targetParent, JsonNode.class));
        return mapper.treeToValue(patched,ParentDto.class);
    }

    @GetMapping("/parents")
    @Operation(summary = "Get a hello message")
    public ResponseEntity<List<ParentDto>> getParents() {

        return ResponseEntity.ok(service.getAllParents());
    }

    @PostMapping("/books")
    @Operation(summary = "Get a hello message")
    public List<Book> getBooks(@RequestBody Book book) {

        return this.bookService.getAllBooks();
    }

    @PostMapping("/patient")
    @Operation(summary = "Get a hello message")
    public PatientDto createPatient(@RequestBody PatientDto patient) {
        return service.createPatient(patient);

    }

    @GetMapping("/patients")
    @Operation(summary = "Get a hello message")
    public List<PatientDto> getAllPatients() {

        return this.service.getAllPatients();
    }



    @PatchMapping ("/patient")
    @Operation(summary = "Get a hello message")
    public PatientDto update(@RequestBody PatientDto patient) {

        return this.service.updatePatient(patient);
    }


    @PostMapping("/rx")
    @Operation(summary = "Get a hello message")
    public PrescriptionEntity createRx() {

         this.service.createPrescription();

         return new PrescriptionEntity();

    }

    @GetMapping("/rx/{id}")
    @Operation(summary = "Get a hello message")
    public PrescriptionEntity getRxById(@PathVariable Long id) {

        return this.service.getRxById(id);
    }

    @GetMapping("/rx")
    @Operation(summary = "Get a hello message")
    public List<PrescriptionEntity> all() {

        return this.service.getAllRx();
    }
}
