package com.example.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.example.demo.repository")
public class DemoApplication {

    public static void main(String[] args) throws JsonProcessingException {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
//        final ParentService parentService = context.getBean(ParentService.class);
//
//       ParentDto parent =  parentService.getParentById(1L);
//       List<ChildDto> childList = parent.getChildren();
//
//       childList.forEach(System.out::println);


    }

}
