package com.example.demo.dto;

import lombok.Data;

@Data
public class PatientDto {
    long id;
    String name;
    int age;
    boolean isHuman;
}
