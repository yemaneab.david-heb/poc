package com.example.demo.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ParentDto {
    private Long id;
    private String name;
    private List<ChildDto> children;
}
