package com.example.demo.dto;

import lombok.Data;

@Data
public class ChildDto {
    private Long id;
    private String name;
    private int age;


    private ParentDto parentDto;



    private long getParentId(){
        return parentDto.getId();
    }
}
