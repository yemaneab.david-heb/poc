package com.example.demo.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import java.util.Arrays;

@Configuration
public class SwaggerConfig {

    /**
     * Creates a bean that is used for swagger to define additional information.
     * @param openApiInfo Additional api info to be injected into swagger.
     * @return Additional OpenAPI information regarding the application.
     */
    @Bean
    public OpenAPI openAPI(Info openApiInfo) {
        return new OpenAPI()
                .info(openApiInfo)
                .components(new Components().addSecuritySchemes("bearer-jwt",
                        new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")
                                .in(SecurityScheme.In.HEADER).name("Authorization")))
                .addSecurityItem(
                        new SecurityRequirement().addList("bearer-jwt", Arrays.asList("read", "write")));
    }

    /**
     * Defines the Api Info for the OpenAPI spec with swagger to utilize.
     * @return Info specifically to be used by openAPI to fill out additional information.
     */
    @Bean
    public Info openApiInfo() {
        return new Info()
                .title("Rx Prescriptions Service")
                .description("Prescriptions Service leveraged by the Core-API Team. It manages the pharmacy prescriptions.")
                .version("1.0.0")
                .contact(new Contact()
                        .name("TBD squad")
                        .url("https://doc.site.com")
                        .email("ml.hwe.squad@heb.com")
                );
    }
}
