package com.example.demo.entity.prescription;

import com.example.demo.entity.prescription.enums.FillNoteSubject;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "FILL_NOTE")
@DynamicInsert
public class FillNoteEntity {

    /* PRIMARY KEY */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    /* FOREIGN KEY */
    @ManyToOne
    @JoinColumn(name = "GLOBAL_FILL_ID", referencedColumnName = "GLOBAL_FILL_ID")
    private FillEntity globalFillId;

    @Column(name = "CREATION_TS")
    private Timestamp creationTs;

    @Column(name = "SUBJECT")
    @Enumerated(EnumType.STRING)
    private FillNoteSubject subject;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "USER_ID")
    private String userId;
}
