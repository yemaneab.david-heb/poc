package com.example.demo.entity.prescription.enums;

import jakarta.persistence.AttributeConverter;

import java.util.Arrays;

public enum PaymentMethod {
    NO_PAYMENT_METHOD(0),
    SPECIALTY_PREPAY(1),
    SPECIALTY_PAY_AT_STORE(2),
    COMPOUND_PREPAY(3),
    COMPOUND_PAY_AT_STORE(4);

    private final int numericValue;
    PaymentMethod(int numericValue) {
        this.numericValue = numericValue;
    }

    public static PaymentMethod forValue(int numericValue)  {
        return Arrays.stream(PaymentMethod.values())
            .filter(paymentMethod -> paymentMethod.numericValue == numericValue)
            .findFirst()
            .orElseThrow();
    }

    public static class PaymentMethodAttributeConverter implements AttributeConverter<PaymentMethod, Integer> {

        @Override
        public Integer convertToDatabaseColumn(PaymentMethod attribute) {
            return attribute.numericValue;
        }

        @Override
        public PaymentMethod convertToEntityAttribute(Integer dbData) {
            return PaymentMethod.forValue(dbData);
        }
    }
}
