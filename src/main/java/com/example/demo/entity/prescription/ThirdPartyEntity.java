package com.example.demo.entity.prescription;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
//@IdClass(ThirdPartyEntity.ThirdPartyId.class)
@Table(name = "THIRD_PARTY")
@DynamicInsert
public class ThirdPartyEntity {

    /* PRIMARY KEY */
//    @AllArgsConstructor
//    @NoArgsConstructor
//    @Data
//    public static class ThirdPartyId implements Serializable {
//        private FillEntity fillEntity;
//        private long rank;
//    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

//    @ManyToOne
//    @JoinColumn(name = "GLOBAL_FILL_ID")
//    private FillEntity fillEntity;

    //@Id
    @Column(name = "GLOBAL_FILL_ID", insertable=false, updatable=false)
    private long globalFillId;

    @Column(name = "RANK")
    private long rank;


    @Column(name = "INSURANCE_ID")
    private String insuranceId;
}
