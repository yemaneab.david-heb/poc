package com.example.demo.entity.prescription.enums;

public enum RxOriginCode {
  NOT_SPECIFIED,
  WRITTEN,
  TELEPHONE,
  ELECTRONIC,
  FACSIMILE,
  PHARMACY
}
