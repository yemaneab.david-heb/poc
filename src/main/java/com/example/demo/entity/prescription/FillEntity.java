package com.example.demo.entity.prescription;

import com.example.demo.entity.prescription.enums.FillStatus;
import com.example.demo.entity.prescription.enums.PaymentMethod;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "FILL")
@DynamicInsert
public class FillEntity {

    /* PRIMARY KEY */
    @Id
    @Column(name = "GLOBAL_FILL_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long globalFillId;

    /* FOREIGN KEY */
    @Column(name = "RX_ID",insertable=false, updatable=false)
    private long rxID;

    @Column(name = "CASH_PRICE_DISPENSED")
    private double cashPriceDispensed;

    @Column(name = "CASH_PRICE_PRESCRIBED")
    private double cashPricePrescribed;

    @Column(name = "DAW")
    private int dawCode;

    @Column(name = "DAYS_SUPPLY")
    private int daysSupply;

    @Column(name = "DISPENSED_PRODUCT_ID")
    private int dispensedProductId;

    @Column(name = "DISPENSED_QUANTITY")
    private double dispensedQuantity;

    @Column(name = "DATE_DRUG_EXPIRATION_TS")
    private Timestamp drugExpirationTs;

    @Column(name = "FILLED_TS")
    private Timestamp filledTs;

    @Column(name = "FILL_NUMBER")
    private int fillNumber;

    @Column(name = "FILL_STATUS")
    @Enumerated(EnumType.STRING)
    private FillStatus fillStatus;

    @Column(name = "IS_MODIFY")
    private boolean isModify;

    @Column(name = "IS_INSURANCE_CARD_ON_HAND")
    private boolean insuranceCardOnHand;

    @Column(name = "IS_POS_PAID")
    private boolean isPOSPaid;

    @Column(name = "MODIFY_REQUEST_REASON")
    private String modifyRequestReason;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "globalFillId", fetch = FetchType.EAGER)
    private List<FillNoteEntity> notes;

    @Column(name = "PAYMENT_METHOD")
    @Convert(converter = PaymentMethod.PaymentMethodAttributeConverter.class)
    private PaymentMethod paymentMethod;

    @Column(name = "PATIENT_PAY")
    private double patientPay;

    @Column(name = "PICKUP_CODE")
    private String pickupCode;

    @Column(name = "POS_TS")
    private Timestamp posTs;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "fill", fetch = FetchType.EAGER)
    private POSDetailEntity posDetail;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "GLOBAL_FILL_ID", nullable = false)
    private List<PrescriptionNoteEntity> prescriptionNotes;

    @Column(name = "PRIOR_AUTHORIZATION_CODE")
    private String priorAuthorizationCode;

    @Column(name = "PRIOR_AUTHORIZATION_NUMBER")
    private String priorAuthorizationNumber;

    @Column(name = "REASON_FOR_CREDITMODIFY")
    private String reasonForCreditModify;

    @Column(name = "SERVICE_TS")
    private Timestamp serviceTs;

    @Column(name = "TECHNICIAN_INITIALS")
    private String technicianInitials;

//    @OneToMany(mappedBy = "fillEntity", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<ThirdPartyEntity> thirdPartyPayors = new ArrayList<>();

    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="GLOBAL_FILL_ID")
    private List<ThirdPartyEntity> thirdPartyPayors = new ArrayList<>();

}
