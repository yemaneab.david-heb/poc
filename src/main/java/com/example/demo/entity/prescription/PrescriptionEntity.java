package com.example.demo.entity.prescription;

import com.example.demo.entity.prescription.enums.RxOriginCode;
import com.example.demo.entity.prescription.enums.RxStatus;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * PrescriptionEntity represents a prescription table record.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PRESCRIPTION")
@DynamicInsert
public class PrescriptionEntity {

    /* PRIMARY KEY */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "C2_POST_TS")
    private Timestamp c2PostTs;

    @Column(name = "C2_SERIAL_NUMBER")
    private String c2SerialNumber;

    @Column(name = "CORPORATE_NUMBER")
    private int corporateNumber;

    @Column(name = "DAYS_SUPPLY")
    private int daysSupply;

    @Column(name = "EXPIRATION_TS")
    private Timestamp expirationTs;

//    @OneToMany(mappedBy = "prescriptionEntity", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<FillEntity> fills = new ArrayList<>();


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "RX_ID",referencedColumnName = "ID",nullable = false)
    private List<FillEntity> fills = new ArrayList<>();

    @Column(name = "IS_ELECTRONIC")
    private boolean isElectronic;

    @Column(name = "IS_PRN")
    private boolean isPRN;

    @Column(name = "LEGACY_RX_ID")
    private String legacyRxId;

    @Column(name = "NUMBER_OF_LABELS")
    private int numberOfLabels;

    @Enumerated(EnumType.STRING)
    @Column(name = "ORIGIN_CODE")
    private RxOriginCode originCode;

    @Column(name = "OUT_OF_SUPPLY_TS")
    private Timestamp outOfSupplyTs;

    @Column(name = "PATIENT_ID")
    private int patientId;

    @Column(name = "PRESCRIBED_PRODUCT_ID")
    private int prescribedProductId;

    @Column(name = "PRESCRIBED_QUANTITY")
    private double prescribedQuantity;

    @Column(name = "PRESCRIBER_ID")
    private int prescriberId;

    @Column(name = "PUBLIC_NUMBER")
    private UUID publicNumber;

    @Column(name = "QUANTITY_OWED")
    private int quantityOwed;

    @Column(name = "REFILLS_AUTHORIZED")
    private int refillsAuthorized;

    @Column(name = "RX_NUMBER")
    private int rxNumber;

    @Column(name = "SIG")
    private String sig;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private RxStatus status;

    @Column(name = "WRITTEN_TS")
    private Timestamp writtenTs;
}
