package com.example.demo.entity.prescription.enums;

public enum RxStatus {
  ORIGINAL_FILL,
  REFILLED,
  INACTIVE,
  KILLED,
  FILED
}
