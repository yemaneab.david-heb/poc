package com.example.demo.entity.prescription;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "POS_DETAIL")
@DynamicInsert
public class POSDetailEntity {

    /* PRIMARY KEY */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /* FOREIGN KEY */
    @OneToOne
    @JoinColumn(name = "GLOBAL_FILL_ID", referencedColumnName = "GLOBAL_FILL_ID")
    private FillEntity fill;

    @Column(name = "POS_ORDER_NUMBER")
    private int orderNumber;

    @Column(name = "POST_TS")
    private Timestamp posTs;

    @Column(name = "REFUND")
    private boolean refund;
}
