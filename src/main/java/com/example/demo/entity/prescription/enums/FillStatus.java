package com.example.demo.entity.prescription.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FillStatus {
    VOID("Void"),
    VOIDED("Voided"),
    FILED("Filed"),
    LAST("Last"),
    FILLED("Filled"),
    SPECIALTY("Specialty"),
    ASSEMBLY("Assembly"),
    COMPOUND("Compound"),
    NEED_MODIFICATION("NeedModification"),
    WILL_CALL("WillCall"),
    PENDING_DUR("PendingDur"),
    IN_STORE_AUTO("InStoreAuto"),
    COMPOUND_VERIFY("Compound Verify"),
    VERIFICATION("Verification"),
    DUR_EXCEPTION("DUR Exception"),
    COMPOUND_MFG("Compound Mfg"),
    SPEC_VERIFY("SpecVfy"),
    PENDING_TP("PendingTP"),
    TP_EXCEPTION("TPException"),
    TP_REV_HOLDING("TPRevHolding"),
    TP_REVERSAL_FAIL("TPReversalFail"),
    PENDING_CF("PendingCF"),
    CF_OUT_OF_STOCK("CFOutOfStock"),
    CF_PRE_ASSEMBLY("CF_PreAssembly"),
    CF_IN_ASSEMBLY("CF_InAssembly"),
    CF_VERIFICATION("CF_Verification"),
    CF_POST_ASSEMBLY("CF_PostAssembly"),
    MARC_EN_ROUTE_TO_CF("MARC_EnRouteToCF"),
    UNKNOWN("Unknown");

    private final String description;
    FillStatus(String description) {
        this.description = description;
    }

    @JsonCreator
    public static FillStatus forValue(String value) {
        return Arrays.stream(FillStatus.values())
            .filter(status -> status.description.equals(value))
            .findFirst()
            .orElse(FillStatus.UNKNOWN);
    }

    @JsonValue
    public String toValue() {
        return this.description;
    }
}

