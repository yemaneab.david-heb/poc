package com.example.demo.entity.prescription.enums;

public enum FillNoteSubject {
    WILLCALL_NOTE,
    COUNSELING_RECOMMENDED,
    REASSIGN_NOTE,
    DOUBLE_COUNTED,
    REFILL_DENIED,
    EFAX_APPEND_BACKTAG
}
