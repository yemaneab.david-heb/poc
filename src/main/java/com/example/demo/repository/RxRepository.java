package com.example.demo.repository;

import com.example.demo.entity.prescription.PrescriptionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RxRepository extends JpaRepository<PrescriptionEntity, Long> {
}
