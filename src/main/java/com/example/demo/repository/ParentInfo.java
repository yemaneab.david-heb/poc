package com.example.demo.repository;

import java.util.List;

/**
 * Projection for {@link com.example.demo.entity.Parent}
 */
public interface ParentInfo {
    long getId();

    String getName();

    List<ChildInfo> getChildren();

    /**
     * Projection for {@link com.example.demo.entity.Child}
     */
    interface ChildInfo {
        long getId();

        String getName();

        Integer getAge();
    }
}