package com.example.demo.repository;

import com.example.demo.entity.Parent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ParentRepository extends JpaRepository<Parent, Long> {
    ParentInfo getParentById(long parentId);
}

