package com.example.demo.service;

import com.example.demo.dto.ChildDto;
import com.example.demo.dto.ParentDto;
import com.example.demo.dto.PatientDto;
import com.example.demo.entity.Child;
import com.example.demo.entity.Parent;
import com.example.demo.entity.Patient;
import com.example.demo.entity.prescription.FillEntity;
import com.example.demo.entity.prescription.PrescriptionEntity;
import com.example.demo.entity.prescription.ThirdPartyEntity;
import com.example.demo.entity.prescription.enums.FillStatus;
import com.example.demo.repository.ChildRepository;
import com.example.demo.repository.ParentRepository;
import com.example.demo.repository.PatientRepository;
import com.example.demo.repository.RxRepository;
import jakarta.persistence.EntityManager;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;

@Service
public class TestService {
    private final ParentRepository parentRepository;
    private final ChildRepository childRepository;
    private final PatientRepository patientRepository;

    private final EntityManager entityManager;

    private final RxRepository rxRepository;

    ModelMapper mapper = new ModelMapper();

    @Autowired
    public TestService(ParentRepository parentRepository, ChildRepository childRepository, PatientRepository patientRepository, EntityManager entityManager, EntityManager entityManager1, RxRepository rxRepository) {
        this.childRepository = childRepository;
        this.parentRepository = parentRepository;
        this.patientRepository = patientRepository;
        this.entityManager = entityManager1;
        this.rxRepository = rxRepository;
    }

    public PatientDto updatePatient(PatientDto patientDto){

        patientRepository.save(mapper.map(patientDto, Patient.class));
        return patientDto;
    }

    public PatientDto createPatient(PatientDto patientDto){


       return mapper.map(patientRepository.save(mapper.map(patientDto, Patient.class)), PatientDto.class);


    }
    public void createParentAndChildren() {
        ParentDto parentDto = new ParentDto();

        ChildDto childDto1 = new ChildDto();
        ChildDto childDto2 = new ChildDto();

        //Add children to parent
        parentDto.setChildren(List.of(childDto1,childDto2));

        ModelMapper modelMapper = new ModelMapper();
        Parent parentEntity = modelMapper.map(parentDto, Parent.class);


       parentRepository.save(parentEntity);

       // childRepository.deleteById(55L);




    }

    public void accessParentChildren() {

       // childRepository.findChildEntitiesByParentId(50L).forEach(System.out::println);

//        parentRepository.getParentEntitiesByIdGreaterThan(50L).forEach(p -> {
//            System.out.println(p);
//        });
    }

    public void printChildren(){
        childRepository.findAll().forEach(System.out::println);
    }

    public int getRandomId(){
        return (int)Math.floor(Math.random()*100);
    }

    public List<PatientDto> getAllPatients() {
        List<Patient> patients = patientRepository.findAll();
        return mapper.map(patients, new TypeToken<List<PatientDto>>(){}.getType());
    }


    @Transactional
    public ParentDto updateParent(ParentDto parentDto) {


        Parent parentEntity = mapper.map(parentDto, Parent.class);
        parentEntity = parentRepository.save(parentEntity);

        parentDto = mapper.map(parentEntity, ParentDto.class);

       List<Child> childEntities = childRepository.findAllByParentId(parentEntity.getId());
       List<ChildDto> childDtos = mapper.map(childEntities,new TypeToken<List<ChildDto>>(){}.getType());
//        parentDto.setChildren(childDtos);


      



        return parentDto;
    }

    public List<ParentDto> getAllParents() {
        return mapper.map(parentRepository.findAll(),new TypeToken<List<ParentDto>>(){}.getType());
    }

    public ParentDto getParentById(Long id) {
        Parent parent = parentRepository.findById(id).get();
        parent.getChildren();
        return mapper.map(parent,ParentDto.class);
    }


    public ParentDto getLazyParent(Long id) {
        Parent parent =  entityManager.find(Parent.class, id);
        entityManager.detach(parent);
        return mapper.map(parent,ParentDto.class);
    }

    public void createPrescription(){
////BI-DIRECTIONAL
//        ThirdPartyEntity thirdPartyEntity = new ThirdPartyEntity();
//        Random random = new Random();
//        int id = random.nextInt(1000) + 1;
//        System.err.println(id);
//        thirdPartyEntity.setRank(id);
//        thirdPartyEntity.setInsuranceId(id + " sdfsdfdsf");
//
//
//        FillEntity fillEntity = new FillEntity();
//        fillEntity.setFillStatus(FillStatus.FILLED);
//
//        thirdPartyEntity.setFillEntity(fillEntity);
//
//        PrescriptionEntity prescriptionEntity = new PrescriptionEntity();
//        prescriptionEntity.setCorporateNumber(891);
//        prescriptionEntity.getFills().add(fillEntity);
//
//        fillEntity.getThirdPartyPayors().add(thirdPartyEntity);
//
//        rxRepository.save(prescriptionEntity);





        //UNI-DIRECTIONAL




        //Create ThirdParty
        ThirdPartyEntity thirdPartyEntity = new ThirdPartyEntity();
        Random random = new Random();
        thirdPartyEntity.setRank(random.nextInt(1000) + 1);
         thirdPartyEntity.setInsuranceId(String.valueOf(random.nextInt(1000) + 1));
       // thirdPartyEntity.setGlobalFillId(fillEntity.getGlobalFillId());

        //Create Fill & Prescription
        FillEntity fillEntity = new FillEntity();
        fillEntity.setFillStatus(FillStatus.FILLED);
        fillEntity.getThirdPartyPayors().add(thirdPartyEntity);

        PrescriptionEntity prescriptionEntity = new PrescriptionEntity();
        prescriptionEntity.setCorporateNumber(891);
        prescriptionEntity.getFills().add(fillEntity);
       //  rxRepository.save(prescriptionEntity);

        rxRepository.save(prescriptionEntity);
    }


    public PrescriptionEntity getRxById(long id) {
        return rxRepository.findById(id).orElse(null);
    }

    public List<PrescriptionEntity> getAllRx() {
       return rxRepository.findAll();
    }
}
