package com.example.demo.service;

import com.example.demo.dto.ParentDto;
import com.example.demo.repository.ParentRepository;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ParentService {
    @Autowired
    private ParentRepository parentRepository;

    public ParentDto getParentById(long id){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(parentRepository.findById(id).orElse(null), ParentDto.class);
    }
}
