package com.example.demo.service;

import com.example.demo.dto.ChildDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChildService {

    @Autowired
    private ParentService parentService;

    public List<ChildDto> getChildrenByParentId(long id){
        return parentService.getParentById(id).getChildren();
    }

}
