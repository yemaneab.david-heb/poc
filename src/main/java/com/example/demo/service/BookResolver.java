package com.example.demo.service;

import com.example.demo.entity.Book;
import graphql.kickstart.annotations.GraphQLQueryResolver;

import java.util.List;

@GraphQLQueryResolver
public class BookResolver{

    private final BookService bookService;

    public BookResolver(BookService bookService) {
        this.bookService = bookService;
    }

    public List<Book> books() {
        return bookService.getAllBooks();
    }
}
